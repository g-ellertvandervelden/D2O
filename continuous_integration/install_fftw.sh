#!/bin/bash

pip install cython

apt-get install -y libfftw3-3 libfftw3-bin libfftw3-dev libfftw3-mpi-dev libfftw3-mpi3
git clone -b mpi https://github.com/fredRos/pyFFTW.git
cd pyFFTW/
#export LDFLAGS="-L/usr/include"
#export CFLAGS="-I/usr/include"
CC=mpicc python setup.py build_ext install
cd ..
