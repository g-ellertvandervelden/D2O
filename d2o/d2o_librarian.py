# D2O
# Copyright (C) 2016  Theo Steininger
#
# Author: Theo Steininger
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from builtins import object
from weakref import WeakValueDictionary as weakdict


class _d2o_librarian(object):

    def __init__(self):
        self.library = weakdict()
        self.counter = 0

    def register(self, d2o):
        self.counter += 1
        self.library[self.counter] = d2o
        return self.counter

    def __getitem__(self, key):
        return self.library[key]

d2o_librarian = _d2o_librarian()
