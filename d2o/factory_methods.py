# -*- coding: utf-8 -*-

import numpy as np

from .config import configuration as gc

from .distributed_data_object import distributed_data_object

from .strategies import STRATEGIES

__all__ = ['arange']


def arange(start, stop=None, step=None, dtype=np.int,
           distribution_strategy=gc['default_distribution_strategy']):

    # Check if the distribution_strategy is a global type one
    if distribution_strategy not in STRATEGIES['global']:
        raise ValueError("ERROR: distribution_strategy must be a global one.")

    # parse the start/stop/step/dtype input
    if step is None:
        step = 1
    else:
        step = int(step)
        if step < 1:
            raise ValueError("ERROR: positive step size needed.")

    dtype = np.dtype(dtype)

    if stop is not None:
        try:
            stop = int(stop)
        except(TypeError):
            raise ValueError("ERROR: no valid 'stop' found.")
        try:
            start = int(start)
        except(TypeError):
            raise ValueError("ERROR: no valid 'start' found.")
    else:
        try:
            stop = int(start)
        except(TypeError):
            raise ValueError("ERROR: no valid 'start' found.")
        start = 0

    # create the empty distributed_data_object
    global_shape = (np.ceil(1.*(stop-start)/step), )
    obj = distributed_data_object(global_shape=global_shape,
                                  dtype=dtype,
                                  distribution_strategy=distribution_strategy)

    # fill obj with the local range-data
    local_arange = obj.distributor.get_local_arange(global_start=start,
                                                    global_step=step)
    obj.set_local_data(local_arange, copy=False)

    return obj
