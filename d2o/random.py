# -*- coding: utf-8 -*-

import numpy as np

from .config import configuration as gc,\
                    dependency_injector as gdi

MPI = gdi[gc['mpi_module']]
comm = getattr(MPI, gc['default_comm'])
rank = comm.rank


def seed(seed=None):
    seed = rank if seed is None else np.uint32(seed) + rank
    np.random.seed(seed)
